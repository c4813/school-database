#pragma once
#include <string>
#include <vector>
#include <map>
#include "../lists/lists.hpp"

struct Payload {
    std::string list_name =  "undefined";
    std::vector<std::string> request_params;
    Payload(){};
    Payload(std::string list_name, std::vector<std::string> params) {
        this->list_name = list_name;
        this->request_params = params;
    }
};

typedef void (*Request)(LIST*, Payload);
void help();
void list_request(LIST* list, Payload request);
void add_request(LIST* list, Payload request);
void find_request(LIST* list, Payload request);

void help_or_clear(std::string);
void commitments(LIST* list, Payload request);
void mentors(LIST* list, Payload request);
Request get_request_function(std::string request);
Payload make_payload(std::vector<std::string> str_vec);
void remove_student(LIST* list, Payload request);
