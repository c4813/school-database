// commitments: such as assignment and enrollment
#pragma once
#include <sstream>
#include <vector>
#include "../entity/Entity.hpp"


// Defines an object of type Enrollment
// Students enroll for courses
struct Enrollment : Entity {
    const char code = 'E';
    int student_number, course_number;
    Enrollment(int stud_no, int course_no);
    std::string to_string() override;
};

// Defines an object of type Assignment
// Teachers are assigned to courses
struct Assignment : Entity {
    const char code = 'A';
    int teacher_number, course_number;
    Assignment(int teacher_no, int course_no);
    std::string to_string() override;
};
