#pragma once
#include "../entity/Entity.hpp"
#include "../course/Course.hpp"
#include "../student/Student.hpp"
#include "../teacher/Teacher.hpp"
#include "../commitments/commitments.hpp"

bool is_number(std::string& str);
bool contains_numbers(std::string& str);
int str_to_int(std::string str);

// Base LIST object
struct LIST {
    Entity* head = NULL;
    Entity* tail = NULL;
    void add_entity(Entity* entity);
    void print_list();
    virtual int remove_student(int number) { return 0; };
    virtual Entity* find(int number) { return NULL; }
    virtual void list(std::string number){}
    virtual Entity* find(std::string s_string) { return NULL; }
    virtual Entity* find(std::string student_no, std::string course_no) { return NULL; }
};

// List Objects
struct STUDENTS : LIST {
    Entity* find(int number);
    Entity* find(std::string s_string);
    int remove_student(int number);
};

struct TEACHERS : LIST {
    Entity* find(int number);
    Entity* find(std::string t_string);
};

struct COURSES : LIST {
    Entity* find(int number);
    Entity* find(std::string c_string);
};

struct ENROLLMENTS : LIST {
    void list(std::string number);
    Entity* find(std::string student_no, std::string course_no);
};

struct ASSIGNMENTS : LIST {
    Entity* find(int number);
    Entity* find(std::string teacher_no, std::string course_no);
};

LIST* get_assignments_list();
LIST* get_course_list();
LIST* get_enrollments_list();
LIST* get_students_list();
LIST* get_teachers_list();
