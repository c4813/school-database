#include <boost/test/included/unit_test.hpp>
#include "../cli/cli.hpp"
#include "../factory/factory.hpp"
using namespace boost::unit_test;


int add (int i, int j) { return i + j; }

void test_add_function()
{
    BOOST_CHECK(add(2,2) == 4);
}

void test_is_valid_student_or_teacher_number_function()
{
    BOOST_CHECK(is_valid_student_or_teacher_number("123456") == true);
    BOOST_CHECK(is_valid_student_or_teacher_number("123") == false);
    BOOST_CHECK(is_valid_student_or_teacher_number("000000") == false);
    BOOST_CHECK(is_valid_student_or_teacher_number("979aut") == false);
}

void test_is_valid_course_name_function()
{
    BOOST_CHECK(is_valid_course_name("CAL01") == true);
    BOOST_CHECK(is_valid_course_name("CAL00") == false);
    BOOST_CHECK(is_valid_course_name("01CAL") == false);
}

boost::unit_test::test_suite* init_unit_test_suite( int argc, char* argv[] )
{
    framework::master_test_suite().add(BOOST_TEST_CASE(&test_add_function));
    framework::master_test_suite().add(BOOST_TEST_CASE(&test_is_valid_student_or_teacher_number_function));
    framework::master_test_suite().add(BOOST_TEST_CASE(&test_is_valid_course_name_function));
    return 0;
}
