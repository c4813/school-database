// Course.hpp
#pragma once
#include <sstream>
#include <vector>
#include "../entity/Entity.hpp"

class Course : public Entity
{
public:
    const char code = 'C';
    int course_number;
    std::string course_name; // assumes the name can be changed
    int semester;

    Course() = delete;
    Course(int course_number, std::string course_name, int semester);
    ~Course();
    std::string to_string() override;
};
