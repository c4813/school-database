#include <iostream>
#include <vector>
#include <typeinfo>
#include "lists.hpp"
#include "../entity/Entity.hpp"
#include "../commitments/commitments.hpp"

static COURSES course_list;
static STUDENTS students_list;
static TEACHERS teachers_list;
static ENROLLMENTS enrollments_list;
static ASSIGNMENTS assignments_list;

LIST* get_assignments_list()
{
    return &assignments_list;
}

LIST* get_course_list()
{
    return &course_list;
}

LIST* get_enrollments_list()
{
    return &enrollments_list;
}

LIST* get_students_list()
{
    return &students_list;
}

LIST* get_teachers_list()
{
    return &teachers_list;
}


bool is_number(std::string& str)
{
    for (char const &c : str)
        if (std::isdigit(c) == 0) return false;
    return true;
}

bool contains_numbers(std::string& str)
{
    for (char const &c : str)
        if (std::isdigit(c) != 0) return true;
    return false;
}

int str_to_int(std::string str)
{
    int integer = 0;
    std::istringstream(str) >> integer;
    return integer;
}

void LIST::add_entity(Entity* entity)
{
    if (this->head == NULL) {
        this->head = entity;
        this->tail = entity;
    } else {
        entity->previous = this->tail;
        this->tail->next = entity;
        this->tail = entity;
    }
}

void LIST::print_list()
{
    Entity* entity = this->head;
    while (entity) {
        std::cout << entity->to_string() << std::endl;
        entity = entity->next;
    }
}

// a teacher can be assigned to more than one course, specify teacher and course in search
Entity* ASSIGNMENTS::find(std::string teacher_no, std::string course_no)
{
    Assignment* assignment = (Assignment*)this->head;
    while (assignment) {
        if (assignment->teacher_number == str_to_int(teacher_no) &
            assignment->course_number == str_to_int(course_no))
                return assignment;
        assignment = (Assignment*)assignment->next;
    }
    return NULL;
}

Entity* ASSIGNMENTS::find(int number)
{
    Assignment* assignment = (Assignment*)this->head;
    while (assignment) {
        if (assignment->course_number == number)
            return assignment;
        assignment = (Assignment*)assignment->next;
    }
    return NULL;
}

Entity* COURSES::find(int number)
{
    Course* course = (Course*)this->head;
    while (course) {
        if (course->course_number == number)
            return course;
        course = (Course*)course->next;
    }
    return NULL;
}


Entity* COURSES::find(std::string c_string)
{
    if (is_number(c_string))
        return find(str_to_int(c_string));

    Course* course = (Course*)this->head;
    while (course) {
        if (course->course_name == c_string)
            return course;
        course = (Course*)course->next;
    }
    return NULL;
}

Entity* STUDENTS::find(int number)
{
    Student* student = (Student*)this->head;
    while (student != NULL) {
        if (student->student_number == number)
            return student;
        student = (Student*)student->next;
    }
    return NULL;
}

int STUDENTS::remove_student(int number)
{
    Student* student = (Student*)this->head;
    // when it is the only entity on the list
    if (student->next == NULL & student->previous == NULL) {
        this->head = NULL;
        this->tail = NULL;
        free(student);
        return 1;
    }

    while (student) {
        if (student->student_number == number) {
            break;
        }
        student = (Student*)student->next;
    }
    // found at the head of the list
    if (student->previous == NULL) {
        student->next->previous = NULL;
        this->head = student->next;
        free(student);
        return 1;
    }
    // found b4 the tail of the list
    Student* previous = (Student*)student->previous;
    if (student->next != NULL) {
        previous->next = student->next;
        student->next->previous = previous;
        free(student);
        return 1;
    }
    // found at the tail of the list_tutors
    if (student->next == NULL) {
        this->tail = previous;
        previous->next = NULL;
        free(student);
        return 1;
    }
    return 0;
}

Entity* STUDENTS::find(std::string s_string)
{
    if (is_number(s_string))
        return find(str_to_int(s_string));

    Student* student = (Student*)this->head;
    while (student) {
        if (student->first_name == s_string)
            return student;
        student = (Student*)student->next;
    }
    return NULL;
}

Entity* ENROLLMENTS::find(std::string s_no, std::string c_no)
{
    Enrollment* enrollment = (Enrollment*)this->head;
    while (enrollment) {
        if (enrollment->student_number == str_to_int(s_no) &
            enrollment->course_number == str_to_int(c_no))
                return enrollment;
        enrollment = (Enrollment*)enrollment->next;
    }
    return NULL;
}

void ENROLLMENTS::list(std::string number)
{
    Enrollment* enrollment = (Enrollment*)this->head;
    while (enrollment) {
        if (enrollment->course_number == str_to_int(number))
            std::cout << get_students_list()->find(enrollment->student_number)->to_string() << "\n";
        enrollment = (Enrollment*)enrollment->next;
    }
}

Entity* TEACHERS::find(int number)
{
    Teacher* teacher = (Teacher*)this->head;
    while (teacher) {
        if (teacher->teacher_number == number)
            return teacher;
        teacher = (Teacher*)teacher->next;
    }
    return NULL;
}

Entity* TEACHERS::find(std::string t_string)
{
    if (is_number(t_string))
        return find(str_to_int(t_string));

    Teacher* teacher = (Teacher*)this->head;
    while (teacher) {
        if (teacher->first_name == t_string)
            return teacher;
        teacher = (Teacher*)teacher->next;
    }
    return NULL;
}
