#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <typeinfo>
#include <string>

#include "./factory/factory.hpp"
#include "./lists/lists.hpp"
#include "./cli/cli.hpp"
#define LOG(x) std::cout << x << std::endl

// map list_name to string
static std::map<std::string, LIST*> lists_map = {{"assignments", get_assignments_list()}, {"courses", get_course_list()},
                                                {"enrollments", get_enrollments_list()}, {"students", get_students_list()}, {"teachers", get_teachers_list()}};

// splits a string into a vector of words, space delimited
static std::vector<std::string> vector_from_string(std::string str)
{
    std::vector<std::string> str_vec;
    std::stringstream str_strm(str);
    std::string tmp;

    while (str_strm >> tmp)
        str_vec.push_back(tmp);
    return str_vec;
}

LIST* what_list(char type)
{
    for (auto elem : lists_map)
    {
        if (elem.first.at(0) - 32 == type)
            return lists_map.at(elem.first);
    }
    return NULL;
}

LIST* what_list(std::string type)
{
    std::string tmp = type;
    LIST* list = NULL;
    try {
        list = lists_map.at(type);
    } catch (std::out_of_range e) {
        try {
            list = lists_map.at(type.append("s"));
        } catch (std::out_of_range e) {
            std::cout << "cannot find list with name " << tmp << "\n";
        }
    }
    return list;
}

// Adds a line of string (now vectors) from the text file to list
void add_text_line_to_list(std::vector<std::string> str_vec)
{
    create_function create_entity = create_what(str_vec[0].at(0));
    LIST* list = what_list(str_vec[0].at(0));
    if (list->find(str_vec[1]) == NULL)
        list->add_entity(create_entity(str_vec));
    else std::cout << "already exists, skipped duplicate entry" << "\n";
}


int main()
{
    std::ifstream file("school_db.txt");
    std::string str;

    if (file.is_open()) {
        std::cout << "reading records from file";
        while (std::getline(file, str))
        {
            add_text_line_to_list(vector_from_string(str));
            std::cout << ".";
        }
        std::cout << "\t" << "ok" << std::endl;
        file.close();
    } else std::cout << "error reading from file, school_db.txt not found" << std::endl;

    std::cout << "enter help for available commands" << std::endl;

    while (true)
    {   // get user input
        std::getline(std::cin, str);
        help_or_clear(str);
        std::vector<std::string> input = vector_from_string(str);
        // check user imput and call function
        if (input.size() < 2) continue;
        try {
            // get the list to be queried
            LIST* list = what_list(input[1]);
            // break if user input cannot be mapped to a list;
            if (list == NULL)  continue;
            // get the query type to be called on the list
            Request request = get_request_function(input[0]);
            if (request == NULL) continue;
            Payload payload = make_payload(input);
            // call the function to handle the users request
            request(list, payload);
        } catch (std::out_of_range e) {
            std::cout << "invalid command enter help for available commands" << "\n";
        }
    }
    return 0;
}
