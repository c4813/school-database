// factory handles the creation of a new entity, after checking the supplied args
#include "factory.hpp"
#include <string>
#include <vector>
#include <array>
#include <iostream>
#include <ctype.h>
// functions mapped to list names : string
std::map<std::string, create_function> create_functions_map = {{"assignment", &create_assignment}, {"course", &create_course},
                                                                {"enrollment", &create_enrollment}, {"student", &create_student}, {"teacher", &create_teacher}};
// returns create_* function mapped to string
create_function create_what(std::string type)
{
    try {
        return create_functions_map.at(type);
    } catch (std::out_of_range e) {
        std::cout << "cannot create invalid entity name" << "\n";
    }
    return NULL;
}

// returns create_* function mapped to char
create_function create_what(char type)
{
    for (auto elem : create_functions_map)
    {
        if (elem.first.at(0) - 32 == type)
            return create_functions_map.at(elem.first);
    }
    return NULL;
}

void error(std::string message)
{
    std::cout << "invalid" << " " << message << " " <<  "details" << "\n";
}

// checks if supplied string can be used as teacher / student number
bool is_valid_student_or_teacher_number(std::string number)
{
    return (is_number(number) & number.size() == 6 & str_to_int(number) > 111111);
}

// checks if number can be used as a course number
bool is_valid_course_number(std::string number)
{
    return (is_number(number) && number.size() == 2);
}

bool is_valid_course_name(std::string name)
{
    std::string alphas = name.substr(0, 3);
    std::string num = name.substr(3, 2);
    return (name.size() == 5 && is_number(alphas) == false && is_number(num) == true && str_to_int(num) > 0);
}

bool is_valid_firstname(std::string name)
{
    return (name.size() > 2 && contains_numbers(name) == false);
}

// assumes max of 4 yrs and  2 sem per yr
bool is_valid_semester(std::string number)
{
    return (str_to_int(number) > 0 && str_to_int(number) < 9 );
}

Entity* create_enrollment(std::vector<std::string> enrollment_details)
{   // check if details meet requirements and enrollment does not already exist
    if (enrollment_details.size() == 3 && is_valid_student_or_teacher_number(enrollment_details[1]) && is_valid_course_number(enrollment_details[2]))
        if (get_enrollments_list()->find(enrollment_details[1], enrollment_details[2]) == NULL)
            return new Enrollment(str_to_int(enrollment_details[1]), str_to_int(enrollment_details[2]));
    return NULL;
}

Entity* create_assignment(std::vector<std::string> assignment_details)
{
    if (assignment_details.size() == 3 && is_valid_student_or_teacher_number(assignment_details[1]) && is_valid_course_number(assignment_details[2]))
        if (get_assignments_list()->find(assignment_details[1], assignment_details[2]) == NULL)
            return new Assignment(str_to_int(assignment_details[1]), str_to_int(assignment_details[2]));
    return NULL;
}

Entity* create_course(std::vector<std::string> course_details)
{
    if (course_details.size() == 4 && is_valid_course_number(course_details[1]) && is_valid_course_name(course_details[2]) && is_valid_semester(course_details[3]))
        if (get_course_list()->find(course_details[1]) == NULL)
            return new Course(str_to_int(course_details[1]), course_details[2], str_to_int(course_details[3]));
    return NULL;
}

// student number is 6chars in length and name should be longer than 2 chars
Entity* create_student(std::vector<std::string> student_details)
{
    if (student_details.size() == 3 && is_valid_student_or_teacher_number(student_details[1]) && is_valid_firstname(student_details[2]))
        if (get_students_list()->find(student_details[1]) == NULL)
            return new Student(str_to_int(student_details[1]), student_details[2]);
    return NULL;
}

Entity* create_teacher(std::vector<std::string> teacher_details)
{
    if (teacher_details.size() == 3 && is_valid_student_or_teacher_number(teacher_details[1]) && is_valid_firstname(teacher_details[2]))
        if (get_teachers_list()->find(teacher_details[1]) == NULL)
            return new Teacher(str_to_int(teacher_details[1]), teacher_details[2]);
    return NULL;
}
