#pragma once
#include <string>
#include <vector>
#include <map>
#include "../lists/lists.hpp"
#include "../entity/Entity.hpp"

#include "../course/Course.hpp"
#include "../student/Student.hpp"
#include "../teacher/Teacher.hpp"
#include "../commitments/commitments.hpp"

Entity* create_course(std::vector<std::string> course_details);
Entity* create_student(std::vector<std::string> students_details);
Entity* create_teacher(std::vector<std::string> teachers_details);
Entity* create_enrollment(std::vector<std::string> enrollment_details);
Entity* create_assignment(std::vector<std::string> assignment_details);

typedef Entity* (*create_function)(std::vector<std::string>);
create_function create_what(std::string type);
create_function create_what(char type);
bool is_valid_student_or_teacher_number(std::string number);
bool is_valid_course_number(std::string number);
bool is_valid_course_name(std::string name);
bool is_valid_firstname(std::string name);
bool is_valid_semester(std::string number);
