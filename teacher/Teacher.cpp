
#include <sstream>
#include <iostream>
#include "Teacher.hpp"
#include "../lists/lists.hpp"

// constructor
Teacher::Teacher(int teacher_number, std::string firstname)
{
    this->teacher_number = teacher_number;
    this->first_name = firstname;
}

std::string Teacher::to_string()
{
    std::stringstream ans;
    ans << "\t" << this->code << " " << this->teacher_number << " " << this->first_name;
    return ans.str();
}

// list the courses enrolled in by the student
void Teacher::list_courses()
{
    Assignment* assignment = (Assignment*)get_assignments_list()->head;
    while (assignment) {
        if (assignment->teacher_number == this->teacher_number)
            std::cout << get_course_list()->find(assignment->course_number)->to_string() << "\n";
        assignment = (Assignment*)assignment->next;
    }
}
