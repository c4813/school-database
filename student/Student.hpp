#pragma once
#include <string>
#include "../entity/Entity.hpp"
#include "../lists/lists.hpp"

class Student : public Entity
{
public:
    const char code = 'S';
    int student_number;
    std::string first_name;

    Student(int student_number, std::string firstname);
    Student() = delete;
    ~Student();
    std::string to_string() override;
    void list_tutors() override;
    void list_courses() override;
};
