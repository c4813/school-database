// Student.cpp
#include <sstream>
#include <vector>
#include "Student.hpp"

Student::Student(int student_number, std::string firstname)
{
    this->student_number = student_number;
    this->first_name = firstname;
}

std::string Student::to_string()
{
    std::stringstream ans;
    ans << "\t" << this->code << " " << this->student_number << " " << this->first_name;
    return ans.str();
}

void Student::list_courses()
{
    Enrollment* enrollment = (Enrollment*)get_enrollments_list()->head;
    while (enrollment) {
        if (enrollment->student_number == this->student_number)
            std::cout << get_course_list()->find(enrollment->course_number)->to_string() << "\n";
        enrollment = (Enrollment*)enrollment->next;
    }
}

void Student::list_tutors()
{
    Enrollment* enrollment = (Enrollment*)get_enrollments_list()->head;
    LIST* assignments = get_assignments_list();
    LIST* teachers = get_teachers_list();
    while (enrollment) {
        if (enrollment->student_number == this->student_number) {
            Assignment* assignment = (Assignment*)assignments->find(enrollment->course_number);
            Teacher* teacher = (Teacher*)teachers->find(assignment->teacher_number);
            std::cout << teacher->to_string() << "\n";
        }
        enrollment = (Enrollment*)enrollment->next;
    }
}
