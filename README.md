# Mandatory C-Assignment

## Topic: Building a Database using Linked List in C Programming

This exercise is built around a student-teacher-course database. During the exercises, you will build a small program that can initialize this database from a file, print the students, teachers and courses in the database, and also do some queries on the database, such as “Which students are enrolled in CAL I1?”.

The database is a simple version of a course database, and we will only deal with certain types of information. These are listed below.

- A student is identified by: S student_number first_name
- A teacher is identified by: T teacher_number first_name
- A course is identified by: C course_number course_name semester_number
- A student enrolment on a course is identified by: E student_number course_number

A teacher assignment to a course (which teacher is teaching the course) is identified by:

- A teacher_number course_number

A sample from a file will look like this:

- S 123456 Ivan
- S 654321 Georgi
- T 123456 Jesper
- T 123457 Ole
- T 123458 Lars
- T 123459 Erland
- C 31 CALI1 3
- C 11 WDDI1 1
- C 21 SDJI2 2
- E 123456 31
- E 123456 11
- E 654321 21
- A 123456 31
- A 123457 11

#### Structuring the information

In order to structure the information, each main type of object (student, course and teacher) must be kept in separate linked lists. On paper, design the C object types that are required to store student information, course information and teacher information.

Ignore data structures for enrolments and teacher-course assignments for now!

Implement the struct-definitions in relevant files and ensure that it compiles. Use typedef’s to make your code more readable.

#### Parsing the input

To get really started, you should write a small main program that can read each record (line) one at a time from the input file and print it to the screen. You can do this by using the “readline” function from a previous exercise in a loop that continuously reads from the standard input (keyboard or redirected to a file) and for each line read calls a function that parses the line read and prints relevant information. In order to parse a line (string), you should use the built-in library function sscanf.

For instance:
`sscanf (line, "S %d %s", &student_number, student_name)`

#### Building lists

In this exercise, you will implement the data structures that will hold the information about students,
courses and teachers, enrolments and assignment. Extend your program with functions to insert a student record from the file into the student list. You will probably need a couple of functions for this.

A function to insert a student_record into the student_list might look something like this:

    `student_node* add_student(student_node *n, int student_number, char* student_name)`

and a function to allocate the required memory for a new studentnode, e.g.

    `student_node* student_node_alloc(void)`

Once you can successfully read all student records from the provided file, you should implement similar functions for courses and teachers.
How are you going to test that the functions work correctly?!

#### Printing the information

Implement a set of recursive functions that will allow you to print out all course-, student-, and teacher-
information from the lists. One such function might have the signature:

    `void print_students (student_node* p)`

Designing for enrolment and teacher assignments
In this exercise, we will look at designing for enrolment and teacher assignments in the system. You can assume that all enrolment and assignment records in the input file come after all students, courses and teachers have been listed.

How are students and courses related? How should the data objects be extended to hold this additional
information?

Adding the enrolment and teacher assignment
Now, implement the extension to the datatypes designed in the previous exercise. Furthermore, add
functions to perform the enrolment of student to courses and assignment of teachers to courses. One such function might look like this:

    `course_node* enrol_student (student_node *student, course_node \*course, int student_number, int course_number)`

As subfunctions you may need to have functions to find the studentNode for a certain student:

    `student_node* find_student (int student_number)`

Make sure that enrol_student returns a “proper” value that you can test upon and for instance print out an error message if the student or course does not exist in your database. Also make sure, that proper error handling is done so that identical enrolment records are properly handled.

#### Quering your database

Implement functions that allow you to list

1. Courses (by number, name and semester) that a given student is enrolled into
2. Courses (by number, name and semester) that a given teacher teaches
3. List of students (number and name) on a given course
4. List of teachers that a student is or has been taught by (hard)

#### Removing students

When a student has completed his study, he/she should be removed from the database.
Implement a function:

    `int remove_student (student_number)`

that removes a student from the DB. Remember to free up the space that was allocated to the student. The function should return 1 if successful (student found and removed), 0 otherwise.

# Solution

This is an attempt to use C++ to create a program that meets some of the requirements.

To implement a data structure and create lists that are searchable is not complex. There is a subtle requirement that "assignments" and "enrollments" should only accept existing students/teachers/courses.

Functions implemented can be listed with the help command.

- 1. To list Courses (by number, name and semester) that a given student is enrolled into: -- courses student [student_number].

- 2. To list Courses (by number, name and semester) that a given teacher teaches: -- courses teacher [teacher_number].

- 3. List of students (number and name) on a given course: -- list enrollments [course_number].

- 4. List of teachers that a student is or has been taught by (hard) -- teachers student [student_number].

- 5. To remove a student from the list: -- remove student [student number]

## Build

    - type make enter while inside the project root dir to build
