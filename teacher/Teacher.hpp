// Teacher.hpp
#pragma once
#include <iostream>
#include <string>
#include "../entity/Entity.hpp"

class Teacher : public Entity
{
public:
    char code = 'T';
    std::string first_name;
    int teacher_number;

    Teacher() = delete;
    Teacher(int teacher_number, std::string firstname);
    ~Teacher();
    std::string to_string() override;
    void list_courses() override;
};
