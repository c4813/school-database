#include <iostream>
#include "cli.hpp"
#include "../factory/factory.hpp"
#include "../lists/lists.hpp"


static std::map<std::string, Request> user_request = {{"list", list_request}, {"add", add_request}, {"find", find_request},
                                                        {"courses", commitments}, {"teachers", mentors}, {"remove", remove_student}};

// strings for help information
static std::string example_commands[] = { "list courses", "list assignments", "list enrollments", "........", "list enrollments [course_number]", "courses student [student_number]",
                                        "courses teacher [teacher_number]", "add course course_number course_name semester", "add teacher teacher_number first_name",
                                        "find assignment teacher_number course_number", "find course [course_name | course_number]", "find enrollment student_number | course_number",
                                        "find student [student_number | first_name]", "find teacher [teacher_number | first_name]", "remove student [student number]" };

void remove_student(LIST* list, Payload request)
{
    if (request.request_params.size() > 1 & list->find(request.request_params[1]) != NULL & is_number(request.request_params[1]))
        if(list->remove_student(str_to_int(request.request_params[1])) == 1)
            std::cout << "student removed successfully" << "\n";
        else  std::cout << "student not removed" << "\n";
}

// list courses a teacher/student commited to. student/teacher number is 6chars
void commitments(LIST* list, Payload request)
{
    if (request.request_params.size() > 1 & request.request_params[1].size() == 6) {
        Entity* entity = list->find(request.request_params[1]);
        if (entity != NULL) entity->list_courses();
    }
}

// handles request to lists all teachers of a student
void mentors(LIST* list, Payload request)
{
    if (request.request_params.size() > 1 & request.request_params[1].size() == 6)
    {
        Entity* entity = list->find(request.request_params[1]);
        if (entity != NULL) entity->list_tutors();
    }
}

// handles request to list every entitiy in a list
void list_request(LIST* list, Payload request)
{
    (request.request_params.size() == 2) ? list->list(request.request_params[1]) : list->print_list();
}

// handles request to add details to a list
void add_request(LIST* list, Payload request)
{
    create_function create = create_what(request.list_name);
    Entity* new_entity = create(request.request_params);
    if (new_entity != NULL)
        list->add_entity(new_entity);
    else std::cout << "invalid details to list" << "\n";
}

// handles request to find an entity in a list
void find_request(LIST* list, Payload request)
{
    Entity* entity;
    if (request.list_name == "assignment" | request.list_name == "enrollment" & request.request_params.size() > 2)
         entity = list->find(request.request_params[1], request.request_params[2]);
    else entity = list->find(request.request_params[1]);
    if (entity != NULL)
        std::cout << entity->to_string() << "\n";
}

// Prints the help information
void help()
{
    std::cout << "\t" << "clear | cl = system clear" << "\n";
    for (auto x : example_commands)
        std::cout << "\t" << x << "\n";
}

Request get_request_function(std::string request)
{
    try {
        return user_request.at(request);
    } catch (std::out_of_range e) {
        try {
            return user_request.at(request.append("s"));
        } catch (std::out_of_range e) {
            std::cout << "input command not valid" << "\n";
        }
    }
    return NULL;
}

Payload make_payload(std::vector<std::string> str_vec)
{
    std::vector<std::string> params = {"#"};
    if (str_vec.size() < 3) return Payload(str_vec[1], params);

    for (int x = 2; x < str_vec.size(); x++)
        params.push_back(str_vec[x]);
    return Payload(str_vec[1], params);
}

// call help or clear screen
void help_or_clear(std::string input)
{
    if (input.empty()) std::cout << "\n";
    else if (input == "help") help();
    else if (input == "clear" | input == "cl") std::system("clear");
}
