// Course.cpp
#include "Course.hpp"
#include <sstream>
#include <vector>
#include <string>
// assume 2 semesters per yr and no course is more than 4 yrs
Course::Course(int course_number, std::string course_name, int semester)
{
    this->course_number = course_number;
    this->course_name = course_name;
    this->semester = semester;
}


// returns a Course to string
std::string Course::to_string()
{
    std::stringstream ans;
    ans << "\t" << this->code << " " << this->course_number << " " <<
        this->course_name << " " << this->semester;
    return ans.str();
}
