// Entity class is the base class
#pragma once


class Entity
{
public:
     Entity* next = NULL;
     Entity* previous = NULL;
     virtual void list_courses() {};
     virtual void list_tutors() {};
     virtual std::string to_string() { return ""; };
};
