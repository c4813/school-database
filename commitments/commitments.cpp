#include "commitments.hpp"
#include <vector>

std::string Assignment::to_string()
{
    std::stringstream ans;
    ans << "\t" << this->code << " " << this->teacher_number << " " << this->course_number;
    return ans.str();
}

std::string Enrollment::to_string()
{
    std::stringstream ans;
    ans << "\t" << this->code << " " << this->student_number << " " << this->course_number;
    return ans.str();
}

Enrollment::Enrollment(int student_number, int course_number)
{
    this->student_number = student_number;
    this->course_number = course_number;
}

Assignment::Assignment(int teacher_number, int course_number)
{    
    this->teacher_number = teacher_number;
    this->course_number = course_number;
}
