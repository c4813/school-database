#Makefile for “SchoolDatabase” C++ application
#Created by Benjamin Major 21/12/21
PROG = SchoolDB
CC = g++
CXXFLAGS = -g -Wall
#LDFLAGS =
OBJS = SchoolDatabase.o
SOURCES = SchoolDatabase.cpp ./student/Student.cpp ./course/Course.cpp \
./teacher/Teacher.cpp ./lists/lists.cpp ./commitments/commitments.cpp ./cli/cli.cpp \
 ./factory/factory.cpp

$(PROG) : $(OBJS)
	 $(CC) -o $(PROG) $(SOURCES)

clean:
	rm -f core $(PROG)
